FROM baserow/web-frontend:1.15.1

USER root

COPY ./plugins/baserow_custom_roles_plugin/ /baserow/plugins/baserow_custom_roles_plugin/
RUN /baserow/plugins/install_plugin.sh --folder /baserow/plugins/baserow_custom_roles_plugin

USER $UID:$GID
