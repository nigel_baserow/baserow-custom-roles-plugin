#!/bin/bash
# Bash strict mode: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail


BASEROW_PLUGIN_DIR=${BASEROW_PLUGIN_DIR:-/baserow/data/plugins}
THIS_PLUGIN_DIR="$BASEROW_PLUGIN_DIR/baserow_custom_roles_plugin"
BASEROW_CUSTOM_ROLES_FILE=${BASEROW_CUSTOM_ROLES_FILE:-"$THIS_PLUGIN_DIR/role_customisations.csv"}