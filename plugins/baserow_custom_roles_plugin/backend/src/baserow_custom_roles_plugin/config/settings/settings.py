import csv
import os
import typing
from pathlib import Path
from collections import defaultdict
from typing import List

if typing.TYPE_CHECKING:
    from baserow.core.registries import OperationType

BASEROW_CUSTOM_ROLES_FILE = os.getenv(
    "BASEROW_CUSTOM_ROLES_FILE",
    "/baserow/data/plugins/baserow_custom_roles_plugin/role_customisations.csv",
)


def load_changes_to_default_roles_from_file(*args, **kwargs):
    """
    Baserow has various default roles available, each of which has an allowed set of
    default operations they can do. This function loads the CSV file
    specified by the BASEROW_CUSTOM_ROLES_FILE env var and adds any specified operations
    to specific default roles.

    It expects the CSV file to be in the following format:

    Role Name, Operation Type Name
    EDITOR,database.table.view.sort.update
    VIEWER,database.table.delete
    """

    changes_file_path = Path(BASEROW_CUSTOM_ROLES_FILE)
    if changes_file_path.is_file():
        with open(changes_file_path, newline="") as open_changes_file:
            parse_and_add_operations_to_default_roles(open_changes_file)
    else:
        print(
            f"Failed to find any custom roles change file at {changes_file_path}, "
            f"skipping default role customization."
        )


def parse_and_add_operations_to_default_roles(open_role_additions_csv):
    changes = _parse_csv_of_role_additions(open_role_additions_csv)
    if changes:
        _add_operations_to_default_roles(changes)


def _add_operations_to_default_roles(changes):
    from baserow_enterprise.role.default_roles import default_roles

    for role, new_ops in changes.items():
        target = default_roles[role]
        for op in new_ops:
            if op not in target:
                target.append(op)
                print(f"{role}: Added {op.type}")


def _parse_csv_of_role_additions(open_changes_file):
    changes = defaultdict(set)
    csv_reader = csv.reader(open_changes_file)
    line_count = 0
    for row in csv_reader:
        line_count += 1
        if line_count == 1:
            continue

        try:
            role, op_type = _parse_csv_line_to_role_and_op(row)
            changes[role].add(op_type)
        except Exception as e:
            print(f"Failed processing row {line_count} because of {e}")

    print(f"Loaded {len(changes)} role customizations to apply.")
    return changes


def _parse_csv_line_to_role_and_op(
    csv_line: List[str],
) -> typing.Tuple[str, "OperationType"]:
    from baserow_enterprise.role.default_roles import default_roles
    from baserow.core.registries import (
        operation_type_registry,
    )

    role = csv_line[0].strip().upper()
    if role not in default_roles:
        raise Exception(
            f"Unknown role name: {role}, must be instead one of {default_roles.keys()}"
        )
    op_name = csv_line[1].strip()
    return role, operation_type_registry.get(op_name)


def setup(settings):
    from django.db.models.signals import post_migrate

    post_migrate.connect(
        load_changes_to_default_roles_from_file,
        dispatch_uid="baserow-custom-roles-plugin",
    )
