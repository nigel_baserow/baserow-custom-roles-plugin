import io
from copy import deepcopy

import pytest

from baserow.contrib.database.views.handler import ViewHandler
from baserow.contrib.database.views.operations import CreateViewOperationType
from baserow.core.exceptions import PermissionDenied
from baserow_custom_roles_plugin.config.settings.settings import (
    parse_and_add_operations_to_default_roles,
)
from baserow_enterprise.apps import sync_default_roles_after_migrate
from baserow_enterprise.role.default_roles import default_roles
from baserow_enterprise.role.handler import RoleAssignmentHandler
from baserow_enterprise.role.models import Role
from django.apps import apps


@pytest.fixture(autouse=True)
def enable_enterprise_for_all_tests_here(enable_enterprise):
    pass

original_editor_roles = deepcopy(default_roles["EDITOR"])

# cd
# PYTHONPATH=$PYTHONPATH:/baserow/enterprise/backend/tests/ pytest
@pytest.mark.django_db(transaction=True)
def test_can_change_editor_to_make_views(data_fixture):
    _reset_cache()
    user = data_fixture.create_user()

    group = data_fixture.create_group()
    database = data_fixture.create_database_application(group=group)
    table = data_fixture.create_database_table(database=database)
    data_fixture.create_user_group(group=group, user=user, permissions="EDITOR")

    sync_default_roles_after_migrate(None, apps=apps)
    with pytest.raises(PermissionDenied):
        ViewHandler().create_view(user, table, "grid", name="test")

    _reset_cache()

    parse_and_add_operations_to_default_roles(
        io.StringIO("role,op\nEDITOR,database.table.create_view")
    )
    assert "database.table.create_view" in {t.type for t in default_roles["EDITOR"]}
    sync_default_roles_after_migrate(None, apps=apps)
    assert (
        Role.objects.get(uid="EDITOR")
        .operations.filter(name="database.table.create_view")
        .exists()
    )

    ViewHandler().create_view(user, table, "grid", name="test")

@pytest.mark.django_db(transaction=True)
def test_can_unknown_role_doesnt_break_permissions(data_fixture):
    _reset_cache()
    user = data_fixture.create_user()

    group = data_fixture.create_group()
    database = data_fixture.create_database_application(group=group)
    table = data_fixture.create_database_table(database=database)
    data_fixture.create_user_group(group=group, user=user, permissions="EDITOR")

    assert "database.table.create_view" not in {t.type for t in default_roles[
        "EDITOR"]}
    sync_default_roles_after_migrate(None, apps=apps)
    assert (
        not Role.objects.get(uid="EDITOR")
        .operations.filter(name="database.table.create_view")
        .exists()
    )
    with pytest.raises(PermissionDenied):
        ViewHandler().create_view(user, table, "grid", name="test")

    _reset_cache()

    parse_and_add_operations_to_default_roles(
        io.StringIO("role,op\nEDITOR2,database.table.create_view")
    )
    assert "EDITOR2" not in default_roles
    sync_default_roles_after_migrate(None, apps=apps)

    with pytest.raises(PermissionDenied):
        ViewHandler().create_view(user, table, "grid", name="test")

@pytest.mark.django_db(transaction=True)
def test_can_unknown_operation_does_not_break_permissions(data_fixture):
    _reset_cache()
    user = data_fixture.create_user()

    group = data_fixture.create_group()
    database = data_fixture.create_database_application(group=group)
    table = data_fixture.create_database_table(database=database)
    data_fixture.create_user_group(group=group, user=user, permissions="EDITOR")

    assert "database.table.create_view" not in {t.type for t in default_roles[
        "EDITOR"]}
    sync_default_roles_after_migrate(None, apps=apps)
    assert (
        not Role.objects.get(uid="EDITOR")
        .operations.filter(name="database.table.create_view")
        .exists()
    )
    with pytest.raises(PermissionDenied):
        ViewHandler().create_view(user, table, "grid", name="test")

    _reset_cache()

    parse_and_add_operations_to_default_roles(
        io.StringIO("role,op\nEDITOR,database.table.create_view2")
    )
    assert "database.table.create_view2" not in {t.type for t in default_roles[
        "EDITOR"]}
    sync_default_roles_after_migrate(None, apps=apps)

    with pytest.raises(PermissionDenied):
        ViewHandler().create_view(user, table, "grid", name="test")


def _reset_cache():
    RoleAssignmentHandler._role_cache_by_uid = {}
    RoleAssignmentHandler._role_cache_by_id = {}
    RoleAssignmentHandler._init = False
    default_roles["EDITOR"] = deepcopy(original_editor_roles)
