# Baserow Custom Roles Plugin

This plugin allows Enterprise Baserow customers to add extra operations to the built-in
default roles Baserow offers:

* ADMIN
* BUILDER
* EDITOR
* VIEWER
* COMMENTER
* NO_ROLE

For example, you could use this plugin to grant all of your Editor's the ability to
also create and work with their own views.

## How to Install

### Cloudron

This section explains how to install on an existing cloudron instance. This plugin only
supports Baserow version 1.14.0 or greater.

#### Step 1) Download the Plugin and upload it into your Cloudron server

First we will download the plugin's files and upload it into your Cloudron server:

1. Download this plugin using the following
   link: https://gitlab.com/nigel_baserow/baserow-custom-roles-plugin/-/archive/main/baserow-custom-roles-plugin-main.zip?path=plugins
2. Extract this ZIP file locally
3. Login to your Cloudron admin page
4. Open up the Cloudron settings for your Baserow instance
5. Click on the File Manager in the top right
6. Click Upload and select Upload Folder
    1. Navigate to where you downloaded and extracted the ZIP file
    2. Click into the extracted folder until you see a folder called "plugins"
    3. Select that folder once and click "Upload"
7. Now you should see in your Cloudron file browser a new "plugins" folder next to the
   other folders like "backups", "caddy", "env" etc.

#### Step 2) Configure your Cloudron to run the plugin on restart

Next we need to configure your Baserow to load this plugin on start-up:

1. Go into the "env" folder in the Cloudron file browser from before
2. Click the "+ New" button, create a new file and call it `plugins.sh`
3. Right click on the `plugins.sh` and select edit
4. Paste in the following file contents into this file and click save:

```
export BASEROW_PLUGIN_DIR=/app/data/plugins
export BASEROW_PLUGIN_SETUP_ALREADY_RUN="yes"
export PYTHONPATH="${PYTHONPATH:-}:/app/data/plugins/baserow_custom_roles_plugin/backend/src"
export BASEROW_CUSTOM_ROLES_FILE="/app/data/plugins/baserow_custom_roles_plugin/presets/editors_can_make_views_preset.csv"
```

5. This by default will load the `editors_can_make_views_preset.csv` preset which
    allows editors to make, edit, delete but not share views.
6. Continue onto Step 3 below.

**Optional**
You can make your own CSV file with your own customizations. You can either edit
the existing `editors_can_make_views_preset.csv` manually in the Cloudron file browser
or by creating your own new `.csv` file.

If you want to make your own new CSV preset:
1. In the cloudron file manager navigate to `plugins/baserow_custom_roles_plugin/presets`
2. Create a new `YOUR_NEW_PRESET_NAME.csv` file
3. Edit it with the operations you'd like to add per default role
   4. See the bottom of this Readme file for all of the available operations and which ones each Baserow role gets by default.
4. Go back to and edit the `env/plugin.sh` file so that the `BASEROW_CUSTOM_ROLES_FILE`
   uses your new file name like so:

```
export BASEROW_CUSTOM_ROLES_FILE="/app/data/plugins/baserow_custom_roles_plugin/presets/YOUR_NEW_PRESET_FILE.csv"
```

#### Step 3) Restart your Cloudron

Now you have installed the plugin you need to restart your Baserow server for any role changes to be applied. 

1. In the Cloudron file manager, find and click the small restart button in the top right.
2. Click "Open Logs" next to the restart button and wait for Baserow to become available again
3. You should see something like the following log lines indicating the customizations were found and loaded:

```
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] Loaded 1 role customizations to apply. (B
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] EDITOR: Added database.table.view.decoration.delete (B
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] EDITOR: Added database.table.view.create_decoration (B
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] EDITOR: Added database.table.view.filter.update (B
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] EDITOR: Added database.table.view.sort.update (B
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] EDITOR: Added database.table.view.update (B
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] EDITOR: Added database.table.view.update_field_options (B
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] EDITOR: Added database.table.view.create_sort (B
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] EDITOR: Added database.table.create_view (B
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] EDITOR: Added database.table.view.create_filter (B
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] EDITOR: Added database.table.view.sort.delete (B
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] EDITOR: Added database.table.view.duplicate (B
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] EDITOR: Added database.table.view.decoration.update (B
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] EDITOR: Added database.table.view.delete (B
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] EDITOR: Added database.table.view.restore (B
Mar 14 10:13:56 [BACKEND][2023-03-14 10:13:56] EDITOR: Added database.table.view.filter.delete (B
```

4. Now your role customizations have been applied.

#### Uninstallation From Cloudron

1. Delete the `plugins/baserow_custom_roles_plugin` folder and `env/plugins.sh` file
2. Restart your Baserow cloudron instance
3. The default roles Baserow ships with will have been restored. 

### Other Installation Methods

Please see https://baserow.io/docs/plugins%2Finstallation for details on how to install
a Baserow plugin in other situations.


## Usage

On startup this plugin will read the CSV file found at the location
specified by the `BASEROW_CUSTOM_ROLES_FILE` env variable.

This CSV is expected to be in the following format:

```
Role Name, Operation Type Name
```

Each row in the CSV will be read by the plugin and the Operation will be added to
the specified role.

## Baserow Defaults 

The following table shows which default roles get which operations by Default in Baserow.
When making your own CSV file you should use the role names and operation types
as you see them in this table.

| Baserow Role Name | Operation Type                                     |
|-------------------|----------------------------------------------------|
| ADMIN             | application.create_snapshot                        |
| ADMIN             | application.delete                                 |
| ADMIN             | application.duplicate                              |
| ADMIN             | application.empty_trash                            |
| ADMIN             | application.list_snapshots                         |
| ADMIN             | application.read                                   |
| ADMIN             | application.read_role                              |
| ADMIN             | application.read_trash                             |
| ADMIN             | application.restore                                |
| ADMIN             | application.snapshot.delete                        |
| ADMIN             | application.snapshot.restore                       |
| ADMIN             | application.update                                 |
| ADMIN             | application.update_role                            |
| ADMIN             | builder.create_page                                |
| ADMIN             | builder.list_pages                                 |
| ADMIN             | builder.order_pages                                |
| ADMIN             | builder.page.delete                                |
| ADMIN             | builder.page.duplicate                             |
| ADMIN             | builder.page.read                                  |
| ADMIN             | builder.page.update                                |
| ADMIN             | database.create_table                              |
| ADMIN             | database.list_tables                               |
| ADMIN             | database.order_tables                              |
| ADMIN             | database.table.create_comment                      |
| ADMIN             | database.table.create_field                        |
| ADMIN             | database.table.create_row                          |
| ADMIN             | database.table.create_view                         |
| ADMIN             | database.table.create_webhook                      |
| ADMIN             | database.table.delete                              |
| ADMIN             | database.table.delete_row                          |
| ADMIN             | database.table.delete_row                          |
| ADMIN             | database.table.delete_row                          |
| ADMIN             | database.table.duplicate                           |
| ADMIN             | database.table.field.delete                        |
| ADMIN             | database.table.field.delete_related_link_row_field |
| ADMIN             | database.table.field.duplicate                     |
| ADMIN             | database.table.field.read                          |
| ADMIN             | database.table.field.restore                       |
| ADMIN             | database.table.field.update                        |
| ADMIN             | database.table.formula.type                        |
| ADMIN             | database.table.import_rows                         |
| ADMIN             | database.table.list_comments                       |
| ADMIN             | database.table.list_fields                         |
| ADMIN             | database.table.list_row_names                      |
| ADMIN             | database.table.list_rows                           |
| ADMIN             | database.table.list_views                          |
| ADMIN             | database.table.list_webhooks                       |
| ADMIN             | database.table.listen_to_all                       |
| ADMIN             | database.table.move_row                            |
| ADMIN             | database.table.order_views                         |
| ADMIN             | database.table.read                                |
| ADMIN             | database.table.read_adjacent_row                   |
| ADMIN             | database.table.read_role                           |
| ADMIN             | database.table.read_row                            |
| ADMIN             | database.table.read_view_order                     |
| ADMIN             | database.table.restore                             |
| ADMIN             | database.table.restore_row                         |
| ADMIN             | database.table.run_export                          |
| ADMIN             | database.table.update                              |
| ADMIN             | database.table.update_role                         |
| ADMIN             | database.table.update_row                          |
| ADMIN             | database.table.view.create_decoration              |
| ADMIN             | database.table.view.create_filter                  |
| ADMIN             | database.table.view.create_sort                    |
| ADMIN             | database.table.view.decoration.delete              |
| ADMIN             | database.table.view.decoration.read                |
| ADMIN             | database.table.view.decoration.update              |
| ADMIN             | database.table.view.delete                         |
| ADMIN             | database.table.view.duplicate                      |
| ADMIN             | database.table.view.filter.delete                  |
| ADMIN             | database.table.view.filter.read                    |
| ADMIN             | database.table.view.filter.update                  |
| ADMIN             | database.table.view.list_aggregations              |
| ADMIN             | database.table.view.list_decoration                |
| ADMIN             | database.table.view.list_filter                    |
| ADMIN             | database.table.view.list_sort                      |
| ADMIN             | database.table.view.read                           |
| ADMIN             | database.table.view.read_aggregation               |
| ADMIN             | database.table.view.read_field_options             |
| ADMIN             | database.table.view.restore                        |
| ADMIN             | database.table.view.sort.delete                    |
| ADMIN             | database.table.view.sort.read                      |
| ADMIN             | database.table.view.sort.update                    |
| ADMIN             | database.table.view.update                         |
| ADMIN             | database.table.view.update_field_options           |
| ADMIN             | database.table.view.update_slug                    |
| ADMIN             | database.table.webhook.delete                      |
| ADMIN             | database.table.webhook.read                        |
| ADMIN             | database.table.webhook.test_trigger                |
| ADMIN             | database.table.webhook.update                      |
| ADMIN             | enterprise.teams.create_subject                    |
| ADMIN             | enterprise.teams.create_team                       |
| ADMIN             | enterprise.teams.delete_subject                    |
| ADMIN             | enterprise.teams.list_subjects                     |
| ADMIN             | enterprise.teams.list_teams                        |
| ADMIN             | enterprise.teams.read_subject                      |
| ADMIN             | enterprise.teams.team.delete                       |
| ADMIN             | enterprise.teams.team.read                         |
| ADMIN             | enterprise.teams.team.restore                      |
| ADMIN             | enterprise.teams.team.update                       |
| ADMIN             | group.assign_role                                  |
| ADMIN             | group.create_application                           |
| ADMIN             | group.create_invitation                            |
| ADMIN             | group.create_token                                 |
| ADMIN             | group.delete                                       |
| ADMIN             | group.empty_trash                                  |
| ADMIN             | group.list_applications                            |
| ADMIN             | group.list_group_users                             |
| ADMIN             | group.list_invitations                             |
| ADMIN             | group.order_applications                           |
| ADMIN             | group.read                                         |
| ADMIN             | group.read_role                                    |
| ADMIN             | group.read_trash                                   |
| ADMIN             | group.restore                                      |
| ADMIN             | group.run_airtable_import                          |
| ADMIN             | group.token.read                                   |
| ADMIN             | group.token.update                                 |
| ADMIN             | group.token.use                                    |
| ADMIN             | group.update                                       |
| ADMIN             | group_user.delete                                  |
| ADMIN             | group_user.update                                  |
| ADMIN             | invitation.delete                                  |
| ADMIN             | invitation.read                                    |
| ADMIN             | invitation.update                                  |
| BUILDER           | application.create_snapshot                        |
| BUILDER           | application.delete                                 |
| BUILDER           | application.duplicate                              |
| BUILDER           | application.list_snapshots                         |
| BUILDER           | application.read                                   |
| BUILDER           | application.read_trash                             |
| BUILDER           | application.restore                                |
| BUILDER           | application.snapshot.delete                        |
| BUILDER           | application.snapshot.restore                       |
| BUILDER           | application.update                                 |
| BUILDER           | builder.create_page                                |
| BUILDER           | builder.list_pages                                 |
| BUILDER           | builder.order_pages                                |
| BUILDER           | builder.page.delete                                |
| BUILDER           | builder.page.duplicate                             |
| BUILDER           | builder.page.read                                  |
| BUILDER           | builder.page.update                                |
| BUILDER           | database.create_table                              |
| BUILDER           | database.list_tables                               |
| BUILDER           | database.order_tables                              |
| BUILDER           | database.table.create_comment                      |
| BUILDER           | database.table.create_field                        |
| BUILDER           | database.table.create_row                          |
| BUILDER           | database.table.create_view                         |
| BUILDER           | database.table.create_webhook                      |
| BUILDER           | database.table.delete                              |
| BUILDER           | database.table.delete_row                          |
| BUILDER           | database.table.delete_row                          |
| BUILDER           | database.table.duplicate                           |
| BUILDER           | database.table.field.delete                        |
| BUILDER           | database.table.field.delete_related_link_row_field |
| BUILDER           | database.table.field.duplicate                     |
| BUILDER           | database.table.field.read                          |
| BUILDER           | database.table.field.restore                       |
| BUILDER           | database.table.field.update                        |
| BUILDER           | database.table.formula.type                        |
| BUILDER           | database.table.import_rows                         |
| BUILDER           | database.table.list_comments                       |
| BUILDER           | database.table.list_fields                         |
| BUILDER           | database.table.list_row_names                      |
| BUILDER           | database.table.list_rows                           |
| BUILDER           | database.table.list_views                          |
| BUILDER           | database.table.list_webhooks                       |
| BUILDER           | database.table.listen_to_all                       |
| BUILDER           | database.table.move_row                            |
| BUILDER           | database.table.order_views                         |
| BUILDER           | database.table.read                                |
| BUILDER           | database.table.read_adjacent_row                   |
| BUILDER           | database.table.read_row                            |
| BUILDER           | database.table.read_view_order                     |
| BUILDER           | database.table.restore                             |
| BUILDER           | database.table.restore_row                         |
| BUILDER           | database.table.run_export                          |
| BUILDER           | database.table.update                              |
| BUILDER           | database.table.update_row                          |
| BUILDER           | database.table.view.create_decoration              |
| BUILDER           | database.table.view.create_filter                  |
| BUILDER           | database.table.view.create_sort                    |
| BUILDER           | database.table.view.decoration.delete              |
| BUILDER           | database.table.view.decoration.read                |
| BUILDER           | database.table.view.decoration.update              |
| BUILDER           | database.table.view.delete                         |
| BUILDER           | database.table.view.duplicate                      |
| BUILDER           | database.table.view.filter.delete                  |
| BUILDER           | database.table.view.filter.read                    |
| BUILDER           | database.table.view.filter.update                  |
| BUILDER           | database.table.view.list_aggregations              |
| BUILDER           | database.table.view.list_decoration                |
| BUILDER           | database.table.view.list_filter                    |
| BUILDER           | database.table.view.list_sort                      |
| BUILDER           | database.table.view.read                           |
| BUILDER           | database.table.view.read_aggregation               |
| BUILDER           | database.table.view.read_field_options             |
| BUILDER           | database.table.view.restore                        |
| BUILDER           | database.table.view.sort.delete                    |
| BUILDER           | database.table.view.sort.read                      |
| BUILDER           | database.table.view.sort.update                    |
| BUILDER           | database.table.view.update                         |
| BUILDER           | database.table.view.update_field_options           |
| BUILDER           | database.table.view.update_slug                    |
| BUILDER           | database.table.webhook.delete                      |
| BUILDER           | database.table.webhook.read                        |
| BUILDER           | database.table.webhook.test_trigger                |
| BUILDER           | database.table.webhook.update                      |
| BUILDER           | enterprise.teams.list_subjects                     |
| BUILDER           | enterprise.teams.list_teams                        |
| BUILDER           | enterprise.teams.read_subject                      |
| BUILDER           | enterprise.teams.team.read                         |
| BUILDER           | group.create_application                           |
| BUILDER           | group.create_token                                 |
| BUILDER           | group.list_applications                            |
| BUILDER           | group.list_group_users                             |
| BUILDER           | group.order_applications                           |
| BUILDER           | group.read                                         |
| BUILDER           | group.read_trash                                   |
| BUILDER           | group.run_airtable_import                          |
| BUILDER           | group.token.read                                   |
| BUILDER           | group.token.update                                 |
| BUILDER           | group.token.use                                    |
| EDITOR            | application.read                                   |
| EDITOR            | builder.list_pages                                 |
| EDITOR            | builder.order_pages                                |
| EDITOR            | builder.page.read                                  |
| EDITOR            | database.list_tables                               |
| EDITOR            | database.table.create_comment                      |
| EDITOR            | database.table.create_row                          |
| EDITOR            | database.table.delete_row                          |
| EDITOR            | database.table.field.read                          |
| EDITOR            | database.table.import_rows                         |
| EDITOR            | database.table.list_comments                       |
| EDITOR            | database.table.list_fields                         |
| EDITOR            | database.table.list_row_names                      |
| EDITOR            | database.table.list_rows                           |
| EDITOR            | database.table.list_views                          |
| EDITOR            | database.table.listen_to_all                       |
| EDITOR            | database.table.move_row                            |
| EDITOR            | database.table.read                                |
| EDITOR            | database.table.read_adjacent_row                   |
| EDITOR            | database.table.read_row                            |
| EDITOR            | database.table.read_view_order                     |
| EDITOR            | database.table.restore_row                         |
| EDITOR            | database.table.run_export                          |
| EDITOR            | database.table.update_row                          |
| EDITOR            | database.table.view.decoration.read                |
| EDITOR            | database.table.view.filter.read                    |
| EDITOR            | database.table.view.list_aggregations              |
| EDITOR            | database.table.view.list_decoration                |
| EDITOR            | database.table.view.list_filter                    |
| EDITOR            | database.table.view.list_sort                      |
| EDITOR            | database.table.view.read                           |
| EDITOR            | database.table.view.read_aggregation               |
| EDITOR            | database.table.view.read_field_options             |
| EDITOR            | database.table.view.sort.read                      |
| EDITOR            | enterprise.teams.list_subjects                     |
| EDITOR            | enterprise.teams.list_teams                        |
| EDITOR            | enterprise.teams.read_subject                      |
| EDITOR            | enterprise.teams.team.read                         |
| EDITOR            | group.list_applications                            |
| EDITOR            | group.list_group_users                             |
| EDITOR            | group.read                                         |
| COMMENTER         | application.read                                   |
| COMMENTER         | builder.list_pages                                 |
| COMMENTER         | builder.order_pages                                |
| COMMENTER         | builder.page.read                                  |
| COMMENTER         | database.list_tables                               |
| COMMENTER         | database.table.create_comment                      |
| COMMENTER         | database.table.field.read                          |
| COMMENTER         | database.table.list_comments                       |
| COMMENTER         | database.table.list_fields                         |
| COMMENTER         | database.table.list_row_names                      |
| COMMENTER         | database.table.list_rows                           |
| COMMENTER         | database.table.list_views                          |
| COMMENTER         | database.table.listen_to_all                       |
| COMMENTER         | database.table.read                                |
| COMMENTER         | database.table.read_adjacent_row                   |
| COMMENTER         | database.table.read_row                            |
| COMMENTER         | database.table.read_view_order                     |
| COMMENTER         | database.table.view.decoration.read                |
| COMMENTER         | database.table.view.filter.read                    |
| COMMENTER         | database.table.view.list_aggregations              |
| COMMENTER         | database.table.view.list_decoration                |
| COMMENTER         | database.table.view.list_filter                    |
| COMMENTER         | database.table.view.list_sort                      |
| COMMENTER         | database.table.view.read                           |
| COMMENTER         | database.table.view.read_aggregation               |
| COMMENTER         | database.table.view.read_field_options             |
| COMMENTER         | database.table.view.sort.read                      |
| COMMENTER         | enterprise.teams.list_teams                        |
| COMMENTER         | enterprise.teams.team.read                         |
| COMMENTER         | group.list_applications                            |
| COMMENTER         | group.read                                         |
| VIEWER            | application.read                                   |
| VIEWER            | builder.list_pages                                 |
| VIEWER            | builder.order_pages                                |
| VIEWER            | builder.page.read                                  |
| VIEWER            | database.list_tables                               |
| VIEWER            | database.table.field.read                          |
| VIEWER            | database.table.list_fields                         |
| VIEWER            | database.table.list_row_names                      |
| VIEWER            | database.table.list_rows                           |
| VIEWER            | database.table.list_views                          |
| VIEWER            | database.table.listen_to_all                       |
| VIEWER            | database.table.read                                |
| VIEWER            | database.table.read_adjacent_row                   |
| VIEWER            | database.table.read_row                            |
| VIEWER            | database.table.read_view_order                     |
| VIEWER            | database.table.view.decoration.read                |
| VIEWER            | database.table.view.filter.read                    |
| VIEWER            | database.table.view.list_aggregations              |
| VIEWER            | database.table.view.list_decoration                |
| VIEWER            | database.table.view.list_filter                    |
| VIEWER            | database.table.view.list_sort                      |
| VIEWER            | database.table.view.read                           |
| VIEWER            | database.table.view.read_aggregation               |
| VIEWER            | database.table.view.read_field_options             |
| VIEWER            | database.table.view.sort.read                      |
| VIEWER            | enterprise.teams.list_teams                        |
| VIEWER            | enterprise.teams.team.read                         |
| VIEWER            | group.list_applications                            |
| VIEWER            | group.read                                         |
