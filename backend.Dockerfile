FROM baserow/backend:1.15.1

USER root

COPY ./plugins/baserow_custom_roles_plugin/ $BASEROW_PLUGIN_DIR/baserow_custom_roles_plugin/
RUN /baserow/plugins/install_plugin.sh --folder $BASEROW_PLUGIN_DIR/baserow_custom_roles_plugin

USER $UID:$GID
